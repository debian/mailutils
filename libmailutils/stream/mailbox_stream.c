/* GNU Mailutils -- a suite of utilities for electronic mail
   Copyright (C) 2022-2025 Free Software Foundation, Inc.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <mailutils/types.h>
#include <mailutils/errno.h>
#include <mailutils/stream.h>

/*
 * Create and open a flat file (mbox or dotmail) mailbox.
 */
int
mu_mailbox_stream_create (mu_stream_t *pstream, const char *filename, int flags)
{
  int try_mmap = 1;
  int rc;
  
  if (!pstream || !filename)
    return EINVAL;

  if (flags & MU_STREAM_APPEND)
    {
      /*
       * If append mode is requested, convert it to read-write, so that
       * mboxrd/dotmail drivers are able to update the X-IMAPbase header
       * in the first message, if necessary.
       *
       * Don't try to use mmap, as it is highly inefficient in append
       * mode.
       */
      flags = (flags & ~MU_STREAM_APPEND) | MU_STREAM_RDWR;
      try_mmap = 0;
    }
  else
    {
      struct stat st;

      if (flags & MU_STREAM_WRITE)
	flags |= MU_STREAM_READ;
      
      if (stat (filename, &st) == 0)
	{
	  try_mmap = st.st_size > 0;
	}
      else if (errno == ENOENT)
	{
	  try_mmap = 0;
	}
    }

  if (try_mmap)
    {
      rc = mu_mapfile_stream_create (pstream, filename, flags);
      if (rc == 0)
	return 0;
    }
  
  return mu_file_stream_create (pstream, filename, flags);
}

