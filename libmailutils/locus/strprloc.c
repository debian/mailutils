/* GNU Mailutils -- a suite of utilities for electronic mail
   Copyright (C) 2017-2025 Free Software Foundation, Inc.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>. */

#include <mailutils/types.h>
#include <mailutils/locus.h>
#include <mailutils/stream.h>

#define STREAM_TYPE mu_stream_t
#define STREAM_PRINTF mu_stream_printf
#define PRINT_LOCUS_POINT mu_stream_print_locus_point
#define PRINT_LOCUS_RANGE mu_stream_print_locus_range
#include "genprloc.c"

