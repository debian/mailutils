/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         FMT_YYSTYPE
/* Substitute the variable and function names.  */
#define yyparse         fmt_yyparse
#define yylex           fmt_yylex
#define yyerror         fmt_yyerror
#define yydebug         fmt_yydebug
#define yynerrs         fmt_yynerrs
#define yylval          fmt_yylval
#define yychar          fmt_yychar

/* First part of user prologue.  */
#line 1 "mh_fmtgram.y"

/* GNU Mailutils -- a suite of utilities for electronic mail
   Copyright (C) 1999-2025 Free Software Foundation, Inc.

   GNU Mailutils is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Mailutils is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>. */

#include <mh.h>
#include <mh_format.h>
#include <sys/stat.h>
  
int yyerror (const char *s);
int yylex (void);
 
static mu_opool_t tokpool;     /* Temporary token storage */


/* Lexical context */
enum context
  {
    ctx_init,   /* Normal text */
    ctx_if,     /* After %< or %? */
    ctx_expr,   /* Expression within cond */
    ctx_func,   /* after (func */
  };

static enum context *ctx_stack;
size_t ctx_tos;
size_t ctx_max;
 
static inline void
ctx_push (enum context ctx)
{
  if (ctx_tos == ctx_max)
    ctx_stack = mu_2nrealloc (ctx_stack, &ctx_max, sizeof (ctx_stack[0]));
  ctx_stack[ctx_tos++] = ctx;
}

static inline void
ctx_pop (void)
{
  if (ctx_tos == 0)
    {
      yyerror ("out of context");
      abort ();
    }
  ctx_tos--;
}

static inline enum context
ctx_get (void)
{
  return ctx_stack[ctx_tos-1];
}

enum node_type
{
  fmtnode_print,
  fmtnode_literal,
  fmtnode_number,
  fmtnode_body,
  fmtnode_comp,
  fmtnode_funcall,
  fmtnode_cntl,
  fmtnode_typecast,
};

struct node
{
  enum node_type nodetype;
  enum mh_type datatype;
  int printflag;
  struct node *prev, *next;
  union
  {
    char *str;
    long num;
    struct node *arg;
    struct
    {
      int fmtspec;
      struct node *arg;
    } prt;
    struct
    {
      mh_builtin_t *builtin;
      struct node *arg;
    } funcall;
    struct
    {
      struct node *cond;
      struct node *iftrue;
      struct node *iffalse;
    } cntl;
  } v;
};

static struct node *parse_tree;
static struct node *new_node (enum node_type nodetype, enum mh_type datatype);

static struct node *printelim (struct node *root);
static void codegen (mh_format_t *fmt, int tree);
static struct node *typecast (struct node *node, enum mh_type type);
 

#line 195 "mh_fmtgram.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif


/* Debug traces.  */
#ifndef FMT_YYDEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define FMT_YYDEBUG 1
#  else
#   define FMT_YYDEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define FMT_YYDEBUG 1
# endif /* ! defined YYDEBUG */
#endif  /* ! defined FMT_YYDEBUG */
#if FMT_YYDEBUG
extern int fmt_yydebug;
#endif

/* Token kinds.  */
#ifndef FMT_YYTOKENTYPE
# define FMT_YYTOKENTYPE
  enum fmt_yytokentype
  {
    FMT_YYEMPTY = -2,
    FMT_YYEOF = 0,                 /* "end of file"  */
    FMT_YYerror = 256,             /* error  */
    FMT_YYUNDEF = 257,             /* "invalid token"  */
    NUMBER = 258,                  /* "number"  */
    STRING = 259,                  /* "string"  */
    COMPONENT = 260,               /* "component"  */
    ARGUMENT = 261,                /* "argument"  */
    FUNCTION = 262,                /* "function name"  */
    IF = 263,                      /* "%<"  */
    ELIF = 264,                    /* "%?"  */
    ELSE = 265,                    /* "%|"  */
    FI = 266,                      /* "%>"  */
    FMTSPEC = 267,                 /* "format specifier"  */
    BOGUS = 268,                   /* BOGUS  */
    EOFN = 269                     /* ")"  */
  };
  typedef enum fmt_yytokentype fmt_yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined FMT_YYSTYPE && ! defined FMT_YYSTYPE_IS_DECLARED
union FMT_YYSTYPE
{
#line 117 "mh_fmtgram.y"

  char *str;
  char const *mesg;
  long num;
  struct {
    struct node *head, *tail;
  } nodelist;
  struct node *nodeptr;
  mh_builtin_t *builtin;
  int fmtspec;
  struct {
    enum mh_type type;
    union
    {
      char *str;
      long num;
    } v;
  } arg;

#line 284 "mh_fmtgram.c"

};
typedef union FMT_YYSTYPE FMT_YYSTYPE;
# define FMT_YYSTYPE_IS_TRIVIAL 1
# define FMT_YYSTYPE_IS_DECLARED 1
#endif


extern FMT_YYSTYPE fmt_yylval;


int fmt_yyparse (void);



/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_NUMBER = 3,                     /* "number"  */
  YYSYMBOL_STRING = 4,                     /* "string"  */
  YYSYMBOL_COMPONENT = 5,                  /* "component"  */
  YYSYMBOL_ARGUMENT = 6,                   /* "argument"  */
  YYSYMBOL_FUNCTION = 7,                   /* "function name"  */
  YYSYMBOL_IF = 8,                         /* "%<"  */
  YYSYMBOL_ELIF = 9,                       /* "%?"  */
  YYSYMBOL_ELSE = 10,                      /* "%|"  */
  YYSYMBOL_FI = 11,                        /* "%>"  */
  YYSYMBOL_FMTSPEC = 12,                   /* "format specifier"  */
  YYSYMBOL_BOGUS = 13,                     /* BOGUS  */
  YYSYMBOL_EOFN = 14,                      /* ")"  */
  YYSYMBOL_YYACCEPT = 15,                  /* $accept  */
  YYSYMBOL_input = 16,                     /* input  */
  YYSYMBOL_list = 17,                      /* list  */
  YYSYMBOL_item = 18,                      /* item  */
  YYSYMBOL_escape = 19,                    /* escape  */
  YYSYMBOL_printable = 20,                 /* printable  */
  YYSYMBOL_component = 21,                 /* component  */
  YYSYMBOL_funcall = 22,                   /* funcall  */
  YYSYMBOL_fmtspec = 23,                   /* fmtspec  */
  YYSYMBOL_function = 24,                  /* function  */
  YYSYMBOL_argument = 25,                  /* argument  */
  YYSYMBOL_cntl = 26,                      /* cntl  */
  YYSYMBOL_zlist = 27,                     /* zlist  */
  YYSYMBOL_if = 28,                        /* if  */
  YYSYMBOL_fi = 29,                        /* fi  */
  YYSYMBOL_elif = 30,                      /* elif  */
  YYSYMBOL_cond = 31,                      /* cond  */
  YYSYMBOL_cond_expr = 32,                 /* cond_expr  */
  YYSYMBOL_elif_part = 33,                 /* elif_part  */
  YYSYMBOL_elif_list = 34,                 /* elif_list  */
  YYSYMBOL_else_part = 35                  /* else_part  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined FMT_YYSTYPE_IS_TRIVIAL && FMT_YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  11
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   36

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  15
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  21
/* YYNRULES -- Number of rules.  */
#define YYNRULES  34
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  44

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   269


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14
};

#if FMT_YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   156,   156,   162,   166,   175,   182,   185,   186,   199,
     200,   203,   215,   315,   318,   321,   328,   331,   349,   356,
     366,   369,   372,   378,   384,   391,   399,   400,   404,   407,
     408,   414,   422,   434,   442
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "\"number\"",
  "\"string\"", "\"component\"", "\"argument\"", "\"function name\"",
  "\"%<\"", "\"%?\"", "\"%|\"", "\"%>\"", "\"format specifier\"", "BOGUS",
  "\")\"", "$accept", "input", "list", "item", "escape", "printable",
  "component", "funcall", "fmtspec", "function", "argument", "cntl",
  "zlist", "if", "fi", "elif", "cond", "cond_expr", "elif_part",
  "elif_list", "else_part", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-29)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-17)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      11,   -29,   -29,   -29,    24,     1,   -29,   -29,    15,   -29,
      15,   -29,   -29,   -29,   -29,   -29,   -29,   -29,     4,   -29,
     -29,    -1,   -29,   -29,   -29,    14,    -1,    17,   -29,   -29,
      -1,    15,    18,    17,   -29,   -29,    -1,   -29,   -29,    15,
     -29,   -29,    -1,   -29
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
      13,     5,    22,    14,     0,    13,     3,     6,     0,     7,
       0,     1,     4,    11,    15,     8,     9,    10,    13,    26,
      27,    20,    25,    17,    18,     0,    21,    28,    12,    24,
      20,     0,     0,    30,    29,    34,    20,    23,    19,     0,
      33,    31,    20,    32
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -29,   -29,    30,    -5,    13,   -29,    25,    26,   -29,   -29,
     -29,   -29,   -28,   -29,   -29,     2,   -14,   -29,   -29,   -29,
       3
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,     4,    26,     6,     7,    15,    19,    20,     8,    18,
      25,     9,    27,    10,    38,    31,    21,    22,    32,    33,
      34
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int8 yytable[] =
{
      12,    -2,    35,     1,   -13,     1,   -13,     2,    41,     2,
      23,     3,     2,     3,    43,     1,     3,    36,   -16,     2,
      13,    12,    14,     3,    11,    42,    29,    30,    28,    37,
       5,    24,     0,    16,    17,    39,    40
};

static const yytype_int8 yycheck[] =
{
       5,     0,    30,     4,     5,     4,     7,     8,    36,     8,
       6,    12,     8,    12,    42,     4,    12,    31,    14,     8,
       5,    26,     7,    12,     0,    39,     9,    10,    14,    11,
       0,    18,    -1,     8,     8,    33,    33
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     4,     8,    12,    16,    17,    18,    19,    23,    26,
      28,     0,    18,     5,     7,    20,    21,    22,    24,    21,
      22,    31,    32,     6,    19,    25,    17,    27,    14,     9,
      10,    30,    33,    34,    35,    27,    31,    11,    29,    30,
      35,    27,    31,    27
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    15,    16,    17,    17,    18,    18,    19,    19,    20,
      20,    21,    22,    23,    23,    24,    25,    25,    25,    26,
      27,    27,    28,    29,    30,    31,    32,    32,    33,    33,
      33,    34,    34,    34,    35
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     1,     2,     1,     1,     1,     2,     1,
       1,     1,     3,     0,     1,     1,     0,     1,     1,     5,
       0,     1,     1,     1,     1,     1,     1,     1,     0,     1,
       1,     3,     4,     2,     2
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = FMT_YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == FMT_YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use FMT_YYerror or FMT_YYUNDEF. */
#define YYERRCODE FMT_YYUNDEF


/* Enable debugging if requested.  */
#if FMT_YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !FMT_YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !FMT_YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yysymbol_kind_t yytoken;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  int yyn = yypact[+*yyctx->yyssp];
  if (!yypact_value_is_default (yyn))
    {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;
      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yyx;
      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
        if (yycheck[yyx + yyn] == yyx && yyx != YYSYMBOL_YYerror
            && !yytable_value_is_error (yytable[yyx + yyn]))
          {
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = YY_CAST (yysymbol_kind_t, yyx);
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




#ifndef yystrlen
# if defined __GLIBC__ && defined _STRING_H
#  define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
# endif
#endif

#ifndef yystpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define yystpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
# endif
#endif

#ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;
      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
#endif


static int
yy_syntax_error_arguments (const yypcontext_t *yyctx,
                           yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yyctx->yytoken != YYSYMBOL_YYEMPTY)
    {
      int yyn;
      if (yyarg)
        yyarg[yycount] = yyctx->yytoken;
      ++yycount;
      yyn = yypcontext_expected_tokens (yyctx,
                                        yyarg ? yyarg + 1 : yyarg, yyargn - 1);
      if (yyn == YYENOMEM)
        return YYENOMEM;
      else
        yycount += yyn;
    }
  return yycount;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                const yypcontext_t *yyctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  yysymbol_kind_t yyarg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* Actual size of YYARG. */
  int yycount = yy_syntax_error_arguments (yyctx, yyarg, YYARGS_MAX);
  if (yycount == YYENOMEM)
    return YYENOMEM;

  switch (yycount)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        yyformat = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  yysize = yystrlen (yyformat) - 2 * yycount + 1;
  {
    int yyi;
    for (yyi = 0; yyi < yycount; ++yyi)
      {
        YYPTRDIFF_T yysize1
          = yysize + yytnamerr (YY_NULLPTR, yytname[yyarg[yyi]]);
        if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
          yysize = yysize1;
        else
          return YYENOMEM;
      }
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yytname[yyarg[yyi++]]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = FMT_YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == FMT_YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= FMT_YYEOF)
    {
      yychar = FMT_YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == FMT_YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = FMT_YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = FMT_YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* input: list  */
#line 157 "mh_fmtgram.y"
            {
	      parse_tree = (yyvsp[0].nodelist).head;
	    }
#line 1593 "mh_fmtgram.c"
    break;

  case 3: /* list: item  */
#line 163 "mh_fmtgram.y"
            {
	      (yyval.nodelist).head = (yyval.nodelist).tail = (yyvsp[0].nodeptr);
	    }
#line 1601 "mh_fmtgram.c"
    break;

  case 4: /* list: list item  */
#line 167 "mh_fmtgram.y"
            {
	      (yyvsp[0].nodeptr)->prev = (yyvsp[-1].nodelist).tail;
	      (yyvsp[-1].nodelist).tail->next = (yyvsp[0].nodeptr);
	      (yyvsp[-1].nodelist).tail = (yyvsp[0].nodeptr);
	      (yyval.nodelist) = (yyvsp[-1].nodelist);
	    }
#line 1612 "mh_fmtgram.c"
    break;

  case 5: /* item: "string"  */
#line 176 "mh_fmtgram.y"
            {
	      struct node *n = new_node (fmtnode_literal, mhtype_str);
	      n->v.str = (yyvsp[0].str);
	      (yyval.nodeptr) = new_node (fmtnode_print, mhtype_str);
	      (yyval.nodeptr)->v.prt.arg = n;
	    }
#line 1623 "mh_fmtgram.c"
    break;

  case 8: /* escape: fmtspec printable  */
#line 187 "mh_fmtgram.y"
            {
	      if ((yyvsp[0].nodeptr)->printflag & MHA_NOPRINT)
		(yyval.nodeptr) = (yyvsp[0].nodeptr);
	      else
		{
		  (yyval.nodeptr) = new_node (fmtnode_print, (yyvsp[0].nodeptr)->datatype);
		  (yyval.nodeptr)->v.prt.fmtspec = ((yyvsp[0].nodeptr)->printflag & MHA_IGNOREFMT) ? 0 : (yyvsp[-1].fmtspec);
		  (yyval.nodeptr)->v.prt.arg = (yyvsp[0].nodeptr);
		}
	    }
#line 1638 "mh_fmtgram.c"
    break;

  case 11: /* component: "component"  */
#line 204 "mh_fmtgram.y"
            {
	      if (mu_c_strcasecmp ((yyvsp[0].str), "body") == 0)
		(yyval.nodeptr) = new_node (fmtnode_body, mhtype_str);
	      else
		{
		  (yyval.nodeptr) = new_node (fmtnode_comp, mhtype_str);
		  (yyval.nodeptr)->v.str = (yyvsp[0].str);
		}
	    }
#line 1652 "mh_fmtgram.c"
    break;

  case 12: /* funcall: function argument ")"  */
#line 216 "mh_fmtgram.y"
            {
	      struct node *arg;

	      ctx_pop ();

	      arg = (yyvsp[-1].nodeptr);
	      if ((yyvsp[-2].builtin)->argtype == mhtype_none)
		{
		  if (arg)
		    {
		      yyerror ("function doesn't take arguments");
		      YYABORT;
		    }
		}
	      else if (arg == NULL)
		{
		  if ((yyvsp[-2].builtin)->flags & MHA_OPTARG_NIL)
		    {
		      switch ((yyvsp[-2].builtin)->argtype)
			{
			case mhtype_str:
			  arg = new_node (fmtnode_literal, mhtype_str);
			  arg->v.str = "";
			  break;
			  
			case mhtype_num:
			  arg = new_node (fmtnode_number, mhtype_num);
			  arg->v.num = 0;
			  break;
			  
			default:
			  abort ();
			}
		    }
		  else if ((yyvsp[-2].builtin)->flags & MHA_OPTARG)
		    {
		      /* ok - ignore */;
		    }
		  else
		    {
		      yyerror ("required argument missing");
		      YYABORT;
		    }
		}
	      else if ((yyvsp[-2].builtin)->flags & MHA_LITERAL)
		{
		  switch ((yyvsp[-2].builtin)->argtype)
		    {
		    case mhtype_num:
		      if (arg->nodetype == fmtnode_number)
			/* ok */;
		      else
			{
			  yyerror ("argument must be a number");
			  YYABORT;
			}
		      break;

		    case mhtype_str:
		      if (arg->nodetype == fmtnode_literal)
			/* ok */;
		      else if (arg->nodetype == fmtnode_number)
			{
			  char *s;
			  mu_asprintf (&s, "%ld", arg->v.num);
			  arg->nodetype = fmtnode_literal;
			  arg->datatype = mhtype_str;
			  arg->v.str = s;
			}
		      else
			{
			  yyerror ("argument must be literal");
			  YYABORT;
			}
		      break;

		    default:
		      break;
		    }
		}
	      
	      if ((yyvsp[-2].builtin)->flags & MHA_VOID)
		{
		  (yyvsp[-1].nodeptr)->printflag = MHA_NOPRINT;
		  (yyval.nodeptr) = (yyvsp[-1].nodeptr);
		}
	      else
		{
		  (yyval.nodeptr) = new_node (fmtnode_funcall, (yyvsp[-2].builtin)->type);
		  (yyval.nodeptr)->v.funcall.builtin = (yyvsp[-2].builtin);
		  (yyval.nodeptr)->v.funcall.arg = typecast (arg, (yyvsp[-2].builtin)->argtype);
		  (yyval.nodeptr)->printflag = (yyvsp[-2].builtin)->flags & MHA_PRINT_MASK;
		  if ((yyvsp[-2].builtin)->type == mhtype_none)
		    (yyval.nodeptr)->printflag = MHA_NOPRINT;
		}
	    }
#line 1753 "mh_fmtgram.c"
    break;

  case 13: /* fmtspec: %empty  */
#line 315 "mh_fmtgram.y"
            {
	      (yyval.fmtspec) = 0;
	    }
#line 1761 "mh_fmtgram.c"
    break;

  case 15: /* function: "function name"  */
#line 322 "mh_fmtgram.y"
            {
	      ctx_push (ctx_func);
	    }
#line 1769 "mh_fmtgram.c"
    break;

  case 16: /* argument: %empty  */
#line 328 "mh_fmtgram.y"
            {
	      (yyval.nodeptr) = NULL;
	    }
#line 1777 "mh_fmtgram.c"
    break;

  case 17: /* argument: "argument"  */
#line 332 "mh_fmtgram.y"
            {
	      switch ((yyvsp[0].arg).type)
		{
		case mhtype_none:
		  (yyval.nodeptr) = NULL;
		  break;
		  
		case mhtype_str:
		  (yyval.nodeptr) = new_node (fmtnode_literal, mhtype_str);
		  (yyval.nodeptr)->v.str = (yyvsp[0].arg).v.str;
		  break;

		case mhtype_num:
		  (yyval.nodeptr) = new_node (fmtnode_number, mhtype_num);
		  (yyval.nodeptr)->v.num = (yyvsp[0].arg).v.num;
		}
	    }
#line 1799 "mh_fmtgram.c"
    break;

  case 18: /* argument: escape  */
#line 350 "mh_fmtgram.y"
            {
	      (yyval.nodeptr) = printelim ((yyvsp[0].nodeptr));
	    }
#line 1807 "mh_fmtgram.c"
    break;

  case 19: /* cntl: if cond zlist elif_part fi  */
#line 357 "mh_fmtgram.y"
            {
	      (yyval.nodeptr) = new_node(fmtnode_cntl, mhtype_num);
	      (yyval.nodeptr)->v.cntl.cond = (yyvsp[-3].nodeptr);
	      (yyval.nodeptr)->v.cntl.iftrue = (yyvsp[-2].nodelist).head;
	      (yyval.nodeptr)->v.cntl.iffalse = (yyvsp[-1].nodeptr);
	    }
#line 1818 "mh_fmtgram.c"
    break;

  case 20: /* zlist: %empty  */
#line 366 "mh_fmtgram.y"
            {
	      (yyval.nodelist).head = (yyval.nodelist).tail = NULL;
	    }
#line 1826 "mh_fmtgram.c"
    break;

  case 22: /* if: "%<"  */
#line 373 "mh_fmtgram.y"
            {
	      ctx_push (ctx_if);
	    }
#line 1834 "mh_fmtgram.c"
    break;

  case 23: /* fi: "%>"  */
#line 379 "mh_fmtgram.y"
            {
	      ctx_pop ();
	    }
#line 1842 "mh_fmtgram.c"
    break;

  case 24: /* elif: "%?"  */
#line 385 "mh_fmtgram.y"
            {
	      ctx_pop ();
	      ctx_push (ctx_if);
	    }
#line 1851 "mh_fmtgram.c"
    break;

  case 25: /* cond: cond_expr  */
#line 392 "mh_fmtgram.y"
            {
	      ctx_pop ();
	      ctx_push (ctx_expr);
	      (yyval.nodeptr) = printelim ((yyvsp[0].nodeptr));
	    }
#line 1861 "mh_fmtgram.c"
    break;

  case 28: /* elif_part: %empty  */
#line 404 "mh_fmtgram.y"
            {
	      (yyval.nodeptr) = NULL;
	    }
#line 1869 "mh_fmtgram.c"
    break;

  case 30: /* elif_part: elif_list  */
#line 409 "mh_fmtgram.y"
            {
	      (yyval.nodeptr) = (yyvsp[0].nodelist).head;
	    }
#line 1877 "mh_fmtgram.c"
    break;

  case 31: /* elif_list: elif cond zlist  */
#line 415 "mh_fmtgram.y"
            {
	      struct node *np = new_node (fmtnode_cntl, mhtype_num);
	      np->v.cntl.cond = (yyvsp[-1].nodeptr);
	      np->v.cntl.iftrue = (yyvsp[0].nodelist).head;
	      np->v.cntl.iffalse = NULL;
	      (yyval.nodelist).head = (yyval.nodelist).tail = np;
	    }
#line 1889 "mh_fmtgram.c"
    break;

  case 32: /* elif_list: elif_list elif cond zlist  */
#line 423 "mh_fmtgram.y"
            {
	      struct node *np = new_node(fmtnode_cntl, mhtype_num);
	      np->v.cntl.cond = (yyvsp[-1].nodeptr);
	      np->v.cntl.iftrue = (yyvsp[0].nodelist).head;
	      np->v.cntl.iffalse = NULL;

	      (yyvsp[-3].nodelist).tail->v.cntl.iffalse = np;
	      (yyvsp[-3].nodelist).tail = np;

	      (yyval.nodelist) = (yyvsp[-3].nodelist);
	    }
#line 1905 "mh_fmtgram.c"
    break;

  case 33: /* elif_list: elif_list else_part  */
#line 435 "mh_fmtgram.y"
            {
	      (yyvsp[-1].nodelist).tail->v.cntl.iffalse = (yyvsp[0].nodeptr);
	      (yyvsp[-1].nodelist).tail = (yyvsp[0].nodeptr);
	      (yyval.nodelist) = (yyvsp[-1].nodelist);
	    }
#line 1915 "mh_fmtgram.c"
    break;

  case 34: /* else_part: "%|" zlist  */
#line 443 "mh_fmtgram.y"
            {
	      (yyval.nodeptr) = (yyvsp[0].nodelist).head;
	    }
#line 1923 "mh_fmtgram.c"
    break;


#line 1927 "mh_fmtgram.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == FMT_YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yytoken};
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == -1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (yymsg)
              {
                yysyntax_error_status
                  = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
                yymsgp = yymsg;
              }
            else
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = YYENOMEM;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == YYENOMEM)
          YYNOMEM;
      }
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= FMT_YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == FMT_YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = FMT_YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != FMT_YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
  return yyresult;
}

#line 448 "mh_fmtgram.y"


static char *start;
static char *tok_start;
static char *curp;
static mu_linetrack_t trk;
static struct mu_locus_range yylloc;

static inline size_t
token_leng (void)
{
  return curp - tok_start;
}

static inline void
mark (void)
{
  if (curp > tok_start)
    mu_linetrack_advance (trk, &yylloc, tok_start, token_leng ());
  tok_start = curp;
}

static inline int
input (void)
{
  if (*curp == 0)
    return 0;
  return *curp++;
}

static inline void
eatinput (size_t n)
{
  mark ();
  while (n--)
    input ();
  mark ();
}

static inline int
peek (void)
{
  return *curp;
}

static inline int
unput (int c)
{
  if (curp == start)
    {
      mu_error (_("%s:%d: INTERNAL ERROR: out of unput space: please report"),
		__FILE__, __LINE__);
      abort ();
    }
  return *--curp = c;
}

static int
skip (int class)
{
  curp = mu_str_skip_class (curp, class);
  return *curp;
}

static int
skipeol (void)
{
  int c;

  do
    {
      c = input ();
      if (c == '\\' && (c = input ()) == '\n')
	c = input ();
    }
  while (c && c != '\n');
  return *curp;
}
	 

static inline int
bogus (const char *mesg)
{
  yylval.mesg = mesg;
  return BOGUS;
}

static char *
find_bol (unsigned line)
{
  char *p = start;

  while (--line)
    {
      while (*p != '\n')
	{
	  if (*p == 0)
	    return p;
	  p++;
	}
      p++;
    }
  return p;
}	      
  
int
yyerror (const char *s)
{
  if (yychar != BOGUS)
    {
      char *bol;
      size_t len;
      static char tab[] = "        ";
      size_t b = 0, e = 0;
      size_t i;
      
      bol = find_bol (yylloc.beg.mu_line);
      len = strcspn (bol, "\n");
      
      mu_diag_at_locus_range (MU_DIAG_ERROR, &yylloc, "%s", s);
      for (i = 0; i < len; i++)
	/* How ... tribal! */
	{
	  if (bol[i] == '\t')
	    {
	      mu_stream_write (mu_strerr, tab, strlen (tab), NULL);
	      if (yylloc.beg.mu_col > i)
		b += strlen (tab) - 1;
	      if (yylloc.end.mu_col > i)
		e += strlen (tab) - 1;
	    }
	  else
	    mu_stream_write (mu_strerr, bol + i, 1, NULL);
	}
      mu_stream_write (mu_strerr, "\n", 1, NULL);
      if (mu_locus_point_eq (&yylloc.beg, &yylloc.end))
	mu_error ("%*.*s^",
		  (int) (b + yylloc.beg.mu_col - 1),
		  (int) (b + yylloc.beg.mu_col - 1), "");
      else
	mu_error ("%*.*s^%*.*s^",
		  (int)(b + yylloc.beg.mu_col - 1),
		  (int)(b + yylloc.beg.mu_col - 1), "",
		  (int)(e + yylloc.end.mu_col - yylloc.beg.mu_col - b - 1),
		  (int)(e + yylloc.end.mu_col - yylloc.beg.mu_col - b - 1),
		  "");
    }
  return 0;
}

static int backslash(int c);

struct lexer_tab
{
  char *ctx_name;
  int (*lexer) (void);
};

static int yylex_initial (void);
static int yylex_cond (void);
static int yylex_expr (void);
static int yylex_func (void);

static struct lexer_tab lexer_tab[] = {
  [ctx_init] = { "initial",    yylex_initial },
  [ctx_if]   = { "condition",  yylex_cond },
  [ctx_expr] = { "expression", yylex_expr },
  [ctx_func] = { "function",   yylex_func }
};
  
int
yylex (void)
{
  int tok;

  do
    {
      mark ();
      if (yydebug)
	fprintf (stderr, "lex: [%s] at %-10.10s...]\n",
		 lexer_tab[ctx_get ()].ctx_name, curp);
      tok = lexer_tab[ctx_get ()].lexer ();
    }
  while (tok == STRING && yylval.str[0] == 0);
  
  mark ();
  if (tok == BOGUS)
    yyerror (yylval.mesg);
  return tok;
}

static int
token_fmtspec (int flags)
{
  int num = 0;
  
  if (peek () == '0')
    {
      flags |= MH_FMT_ZEROPAD;
      input ();
    }
  else if (!mu_isdigit (peek ()))
    {
      return bogus ("expected digit");
    }
  mark ();
  while (*curp && mu_isdigit (peek ()))
    num = num * 10 + input () - '0';
  yylval.fmtspec = flags | num;
  unput ('%');
  return FMTSPEC;
}

static int
token_function (void)
{
  eatinput (1);
  skip (MU_CTYPE_IDENT);
  if (token_leng () == 0 || !strchr (" \t(){%", peek ()))
    {
      return bogus ("expected function name");
    }

  yylval.builtin = mh_lookup_builtin (tok_start, token_leng ());

  if (!yylval.builtin)
    {
      return bogus ("unknown function");
    }
  if (!yylval.builtin->fun
      && !(yylval.builtin->flags & (MHA_SPECIAL|MHA_VOID)))
    {
      mu_error ("INTERNAL ERROR at %s:%d: \"%s\" has no associated function"
		" and is not marked as MHA_SPECIAL",
		__FILE__, __LINE__, yylval.builtin->name);
      abort ();
    }
  
  return FUNCTION;
}
  
static int
token_component (void)
{
  eatinput (1);
  if (!mu_isalpha (peek ()))
    {
      return bogus ("component name expected");
    }
  mark ();
  if (skip (MU_CTYPE_HEADR) != '}')
    {
      return bogus ("component name expected");
    }
  mu_opool_append (tokpool, tok_start, token_leng ());
  mu_opool_append_char (tokpool, 0);
  yylval.str = mu_opool_finish (tokpool, NULL);
  eatinput (1);
  return COMPONENT;
}

int
yylex_initial (void)
{
  int c;

 again:
  mark ();
  if (peek () == '%')
    {
      input ();

      switch (c = input ())
	{
	case ';':
	  skipeol ();
	  goto again;
	case '<':
	  return IF;
	case '%':
	  unput (c);
	  unput (c);
	  break;
	case '(':
	  unput (c);
	  return token_function ();
	case '{':
	  unput (c);
	  return token_component ();
	case '-':
	  return token_fmtspec (MH_FMT_RALIGN);
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
	  unput (c);
	  return token_fmtspec (MH_FMT_DEFAULT);
	default:
	  return bogus ("component or function name expected");
      }
    }
  
  c = peek ();
  
  if (c == 0)
    return 0;

  while ((c = input ()) != 0)
    {
      if (c == '%')
	{
	  if (peek () == '%')
	    mu_opool_append_char (tokpool, input ());
	  else
	    {
	      unput (c);
	      break;
	    }
	}
      else if (c == '\\')
	{
	  if ((c = input ()) == 0)
	    {
	      return bogus ("unexpected end of file");
	    }
	  if (c != '\n')
	    mu_opool_append_char (tokpool, backslash (c));
	}
      else
	mu_opool_append_char (tokpool, c);
    }

  mu_opool_append_char (tokpool, 0);
  yylval.str = mu_opool_finish (tokpool, NULL);
  return STRING;
}  

int
yylex_cond (void)
{
  while (1)
    {
      switch (peek ())
	{
	case '(':
	  return token_function ();
	case '{':
	  return token_component ();
	case '\\':
	  input ();
	  if (input () == '\n')
	    continue;
	default:
	  return bogus ("'(' or '{' expected");
	}
    }
}

int
yylex_expr (void)
{
  int c;
  
  if ((c = input ()) == '%')
    {
      switch (c = input ())
	{
	case '?':
	  return ELIF;
	case '|':
	  return ELSE;
	case '>':
	  return FI;
	}
      unput (c);
      unput ('%');
    }
  else
    unput (c);
  return yylex_initial ();
}

int
yylex_func (void)
{
  int c;

  /* Expected argument or closing parenthesis */
 again:
  mark ();
  switch (peek ())
    {
    case '(':
      return token_function ();
      
    case ')':
      eatinput (1);
      return EOFN;
      
    case '{':
      return token_component ();

    case '%':
      input ();
      switch (peek ())
	{
	case '<':
	  input ();
	  return IF;

	case '%':
	  break;

	default:
	  return bogus ("expected '%' or '<'");
	}
      break;

    case ' ':
    case '\t':
      skip (MU_CTYPE_SPACE);
      if (peek () == '%')
	goto again;
      break;

    default:
      return input ();
    }

  mark ();

  while ((c = input ()) != ')')
    {
      if (c == 0)
	{
	  return bogus ("expected ')'");
	}
      
      if (c == '\\')
	{
	  if ((c = input ()) == 0)
	    {
	      return bogus ("unexpected end of file");
	    }
	  mu_opool_append_char (tokpool, backslash (c));
	}
      else
	mu_opool_append_char (tokpool, c);
    }
  mu_opool_append_char (tokpool, 0);

  yylval.arg.v.str = mu_opool_finish (tokpool, NULL);
  yylval.arg.type = mhtype_str;
  unput (c);
  
  if (mu_isdigit (yylval.arg.v.str[0])
      || (yylval.arg.v.str[0] == '-' && mu_isdigit (yylval.arg.v.str[1])))
    {
      long n;
      char *p;
      errno = 0;
      n = strtol (yylval.arg.v.str, &p, 0);
      if (errno == 0 && *p == 0)
	{
	  yylval.arg.type = mhtype_num;
	  yylval.arg.v.num = n;
	}
    }

  if (peek () != ')')
    {
      return bogus ("expected ')'");
    }
  
  return ARGUMENT;
}

static int
format_parse (mh_format_t *fmtptr, char *format_str,
	      struct mu_locus_point const *locus,
	      int flags)
{
  int rc;
  char *p = getenv ("MHFORMAT_DEBUG");
  
  if (p || (flags & MH_FMT_PARSE_DEBUG))
    yydebug = 1;
  start = tok_start = curp = format_str;
  mu_opool_create (&tokpool, MU_OPOOL_ENOMEMABRT);

  ctx_tos = ctx_max = 0;
  ctx_stack = NULL;
  ctx_push (ctx_init);
  mu_linetrack_create (&trk, "input", 2);
  if (locus && locus->mu_file)
    mu_linetrack_rebase (trk, locus);
  mu_locus_range_init (&yylloc);
  
  rc = yyparse ();
  if (rc == 0)
    codegen (fmtptr, flags & MH_FMT_PARSE_TREE);
  else
    mu_opool_destroy (&tokpool);

  mu_locus_range_deinit (&yylloc);
  mu_linetrack_destroy (&trk);
  free (ctx_stack);
  
  parse_tree = NULL;
  tokpool = NULL;
  return rc;
}

int
mh_format_string_parse (mh_format_t *retfmt, char const *format_str,
			struct mu_locus_point const *locus,
			int flags)
{
  char *fmts = mu_strdup (format_str);
  int rc = format_parse (retfmt, fmts, locus, flags);
  free (fmts);
  return rc;
}

int
mh_read_formfile (char const *name, char **pformat)
{
  FILE *fp;
  struct stat st;
  char *format_str;
  char *file_name;
  int rc;
  
  rc = mh_find_file (name, &file_name);
  if (rc)
    {
      mu_error (_("cannot access format file %s: %s"), name, strerror (rc));
      return -1;
    }
  
  if (stat (file_name, &st))
    {
      mu_error (_("cannot stat format file %s: %s"), file_name,
		strerror (errno));
      free (file_name);
      return -1;
    }
  
  fp = fopen (file_name, "r");
  if (!fp)
    {
      mu_error (_("cannot open format file %s: %s"), file_name,
		strerror (errno));
      free (file_name);
      return -1;
    }
  
  format_str = mu_alloc (st.st_size + 1);
  if (fread (format_str, st.st_size, 1, fp) != 1)
    {
      mu_error (_("error reading format file %s: %s"), file_name,
		strerror (errno));
      free (file_name);
      return -1;
    }
  free (file_name);
  
  format_str[st.st_size] = 0;
  if (format_str[st.st_size-1] == '\n')
    format_str[st.st_size-1] = 0;
  fclose (fp);
  *pformat = format_str;
  return 0;
}

int
mh_format_file_parse (mh_format_t *retfmt, char const *formfile, int flags)
{
  char *fmts;
  int rc;
  
  rc = mh_read_formfile (formfile, &fmts);
  if (rc == 0)
    {
      struct mu_locus_point loc;
      loc.mu_file = formfile;
      loc.mu_line = 1;
      loc.mu_col = 0;   
      rc = format_parse (retfmt, fmts, &loc, flags);
      free (fmts);
    }
  return rc;
}

int
backslash (int c)
{
  static char transtab[] = "b\bf\fn\nr\rt\t";
  char *p;
  
  for (p = transtab; *p; p += 2)
    {
      if (*p == c)
	return p[1];
    }
  return c;
}

static struct node *
new_node (enum node_type nodetype, enum mh_type datatype)
{
  struct node *np = mu_zalloc (sizeof *np);
  np->nodetype = nodetype;
  np->datatype = datatype;
  return np;
}

static void node_list_free (struct node *node);

static void
node_free (struct node *node)
{
  if (!node)
    return;
  switch (node->nodetype)
    {
    case fmtnode_print:
      node_free (node->v.prt.arg);
      break;

    case fmtnode_literal:
      break;

    case fmtnode_number:
      break;

    case fmtnode_body:
      break;

    case fmtnode_comp:
      break;

    case fmtnode_funcall:
      node_free (node->v.funcall.arg);
      break;

    case fmtnode_cntl:
      node_list_free (node->v.cntl.cond);
      node_list_free (node->v.cntl.iftrue);
      node_list_free (node->v.cntl.iffalse);
      break;

    default:
      abort ();
    }
  free (node);
}

static void
node_list_free (struct node *node)
{
  while (node)
    {
      struct node *next = node->next;
      node_free (node);
      node = next;
    }
}

static struct node *
typecast (struct node *node, enum mh_type type)
{
  if (!node)
    /* FIXME: when passing optional argument, the caller must know the
       type of value returned by the previous expression */
    return node;
  
  if (node->datatype == type)
    return node;
  switch (node->nodetype)
    {
    case fmtnode_cntl:
      node->v.cntl.iftrue = typecast (node->v.cntl.iftrue, type);
      node->v.cntl.iffalse = typecast (node->v.cntl.iffalse, type);
      node->datatype = type;
      break;

    default:
      {
	struct node *arg = new_node (fmtnode_typecast, type);
	arg->v.arg = node;
	node = arg;
      }
    }
  return node;
}

#define INLINE -1

static inline void
indent (int level)
{
  printf ("%*.*s", 2*level, 2*level, "");
}

static inline void
delim (int level, char const *dstr)
{
  if (level == INLINE)
    printf ("%s", dstr);
  else
    {
      printf ("\n");
      indent (level);
    }
}

static void dump_statement (struct node *node, int level);

void
mh_print_fmtspec (int fmtspec)
{
  if (!(fmtspec & (MH_FMT_RALIGN|MH_FMT_ZEROPAD|MH_FMT_COMPWS)))
    printf ("NONE");
  else
    {
      if (!(fmtspec & MH_FMT_RALIGN))
	printf ("NO");
      printf ("RALIGN|");
      if (!(fmtspec & MH_FMT_ZEROPAD))
	printf ("NO");
      printf ("ZEROPAD|");
      if (!(fmtspec & MH_FMT_COMPWS))
	printf ("NO");
      printf ("COMPWS");
    }
}

static char *typename[] = { "NONE", "NUM", "STR" };

static void
dump_node_pretty (struct node *node, int level)
{
  if (!node)
    return;  
  switch (node->nodetype)
    {
    case fmtnode_print:
      if (node->v.prt.fmtspec)
	{
	  printf ("FORMAT(");
	  mh_print_fmtspec (node->v.prt.fmtspec);
	  printf(", %d, ", node->v.prt.fmtspec & MH_WIDTH_MASK);
	}
      else
	printf ("PRINT(");
      dump_statement (node->v.prt.arg, INLINE);
      printf (")");
      break;
      
    case fmtnode_literal:
      {
	char const *p = node->v.str;
	putchar ('"');
	while (*p)
	  {
	    if (*p == '\\' || *p == '"')
	      {
		putchar ('\\');
		putchar (*p);
	      }
	    else if (*p == '\n')
	      {
		putchar ('\\');
		putchar ('n');
	      }
	    else
	      putchar (*p);
	    p++;
	  }
	putchar ('"');
      }	
      break;
      
    case fmtnode_number:
      printf ("%ld", node->v.num);
      break;
      
    case fmtnode_body:
      printf ("BODY");
      break;
      
    case fmtnode_comp:
      printf ("COMPONENT.%s", node->v.str);
      break;
      
    case fmtnode_funcall:
      printf ("%s(", node->v.funcall.builtin->name);
      dump_statement (node->v.funcall.arg, INLINE);
      printf (")");
      break;
      
    case fmtnode_cntl:
      printf ("IF (");
      dump_node_pretty (node->v.cntl.cond, INLINE);
      printf (") THEN");

      if (level != INLINE)
	level++;
      
      delim (level, "; ");

      dump_statement (node->v.cntl.iftrue, level);

      if (node->v.cntl.iffalse)
	{
	  delim (level == INLINE ? level : level - 1, "; ");
	  printf ("ELSE");
	  delim (level, " ");
	  dump_statement (node->v.cntl.iffalse, level);
	}

      if (level != INLINE)
	level--;
      delim (level, "; ");
      printf ("FI");
      break;

    case fmtnode_typecast:
      printf ("%s(", typename[node->datatype]);
      dump_node_pretty (node->v.arg, INLINE);
      printf (")");
      break;

    default:
      abort ();
    }
}

static void
dump_statement (struct node *node, int level)
{
  while (node)
    {
      dump_node_pretty (node, level);
      node = node->next;
      if (node)
	delim (level, "; ");
    }
}

void
mh_format_dump_code (mh_format_t fmt)
{
  dump_statement (fmt->tree, 0);
  printf ("\n");
}

void
mh_format_free_tree (mh_format_t fmt)
{
  if (fmt)
    {
      node_list_free (fmt->tree);
      fmt->tree = NULL;
      mu_opool_destroy (&fmt->pool);
    }
}

void
mh_format_free (mh_format_t fmt)
{
  if (!fmt)
    return;
  
  mh_format_free_tree (fmt);
    
  if (fmt->prog)
    free (fmt->prog);
  fmt->progmax = fmt->progcnt = 0;
  fmt->prog = NULL;
}

void
mh_format_destroy (mh_format_t *fmt)
{
  if (fmt)
    {
      mh_format_free (*fmt);
      *fmt = NULL;
    }
}

static struct node *
printelim (struct node *node)
{
  if (node->nodetype == fmtnode_print)
    {
      struct node *arg = node->v.prt.arg;
      arg->next = node->next;
      free (node);
      node = arg;
    }
  return node;
}

#define PROG_MIN_ALLOC 8

static inline void
ensure_space (struct mh_format *fmt, size_t n)
{
  while (fmt->progcnt + n >= fmt->progmax)
    {
      if (fmt->progmax == 0)
	fmt->progmax = n < PROG_MIN_ALLOC ? PROG_MIN_ALLOC : n;
      fmt->prog = mu_2nrealloc (fmt->prog, &fmt->progmax, sizeof fmt->prog[0]);
    }
}
  
static void
emit_instr (struct mh_format *fmt, mh_instr_t instr)
{
  ensure_space (fmt, 1);
  fmt->prog[fmt->progcnt++] = instr;
}

static inline void
emit_opcode (struct mh_format *fmt, mh_opcode_t op)
{
  emit_instr (fmt, (mh_instr_t) op);
}

static void
emit_string (struct mh_format *fmt, char const *str)
{
  size_t length = strlen (str) + 1;
  size_t count = (length + sizeof (mh_instr_t)) / sizeof (mh_instr_t) + 1;
  
  ensure_space (fmt, count);
  emit_instr (fmt, (mh_instr_t) count);
  memcpy (MHI_STR (fmt->prog[fmt->progcnt]), str, length);
  fmt->progcnt += count;
}

static void codegen_node (struct mh_format *fmt, struct node *node);
static void codegen_nodelist (struct mh_format *fmt, struct node *node);

static void
emit_opcode_typed (struct mh_format *fmt, enum mh_type type,
		   enum mh_opcode opnum, enum mh_opcode opstr)
{
  switch (type)
    {
    case mhtype_num:
      emit_opcode (fmt, opnum);
      break;

    case mhtype_str:
      emit_opcode (fmt, opstr);
      break;

    default:
      abort ();
    }
}

static void
emit_special (struct mh_format *fmt, mh_builtin_t *builtin, struct node *arg)
{
  if (arg)
    {
      if (builtin->flags & MHA_LITERAL)
	{
	  switch (arg->nodetype)
	    {
	    case fmtnode_literal:
	      emit_opcode (fmt, mhop_sets);
	      emit_instr (fmt, (mh_instr_t) (long) R_REG);
	      emit_string (fmt, arg->v.str);
	      break;

	    case fmtnode_number:
	      emit_opcode (fmt, mhop_setn);
	      emit_instr (fmt, (mh_instr_t) (long) R_REG);
	      emit_instr (fmt, (mh_instr_t) (long) arg->v.num);
	      break;

	    default:
	      abort ();
	    }
	}
      else
	codegen_node (fmt, arg);
    }
}

static void
emit_funcall (struct mh_format *fmt, mh_builtin_t *builtin, struct node *arg)
{
  if (builtin->flags & MHA_ACC)
    {
      emit_opcode (fmt, mhop_movs);
      emit_instr (fmt, (mh_instr_t) (long) R_ACC);
      emit_instr (fmt, (mh_instr_t) (long) R_REG);
    }
  
  if (builtin->flags & MHA_SPECIAL)
    {
      emit_special (fmt, builtin, arg);
      return;
    }

  if (arg)
    {
      if (builtin->flags & MHA_LITERAL)
	{
	  switch (arg->nodetype)
	    {
	    case fmtnode_literal:
	      emit_opcode (fmt, mhop_sets);
	      emit_instr (fmt, (mh_instr_t) (long) R_ARG);
	      emit_string (fmt, arg->v.str);
	      break;

	    case fmtnode_number:
	      emit_opcode (fmt, mhop_setn);
	      emit_instr (fmt, (mh_instr_t) (long) R_ARG);
	      emit_instr (fmt, (mh_instr_t) (long) arg->v.num);
	      break;

	    default:
	      abort ();
	    }
	}
      else
	{
	  codegen_node (fmt, arg);
	  emit_opcode_typed (fmt, arg->datatype, mhop_movn, mhop_movs);
	  emit_instr (fmt, (mh_instr_t) (long) R_ARG);
	  emit_instr (fmt, (mh_instr_t) (long) R_REG);
	}
    }
  else if (builtin->argtype != mhtype_none)
    {
      emit_opcode_typed (fmt, builtin->argtype, mhop_movn, mhop_movs);
      emit_instr (fmt, (mh_instr_t) (long) R_ARG);
      emit_instr (fmt, (mh_instr_t) (long) R_REG);
    }

  emit_opcode (fmt, mhop_call);
  emit_instr (fmt, (mh_instr_t) builtin->fun);
}

static void
codegen_node (struct mh_format *fmt, struct node *node)
{
  if (!node)
    return;
  switch (node->nodetype)
    {
    case fmtnode_print:
      if (node->v.prt.arg->nodetype == fmtnode_literal)
	{
	  emit_opcode (fmt, mhop_printlit);
	  emit_string (fmt, node->v.prt.arg->v.str);
	}
      else if (node->v.prt.arg->nodetype == fmtnode_number)
	{
	  char *s;
	  emit_opcode (fmt, mhop_printlit);
	  mu_asprintf (&s, "%ld", node->v.prt.arg->v.num);
	  emit_string (fmt, s);
	  free (s);
	}
      else
	{
	  codegen_node (fmt, node->v.prt.arg);
	  if (node->v.prt.fmtspec)
	    {
	      emit_opcode (fmt, mhop_fmtspec);
	      emit_instr (fmt, (mh_instr_t) (long) node->v.prt.fmtspec);
	    }
	  
	  if (node->v.prt.arg->datatype != mhtype_none)
	    emit_opcode_typed (fmt, node->v.prt.arg->datatype,
			       mhop_printn, mhop_prints);
	}
      break;

    case fmtnode_literal:
      emit_opcode (fmt, mhop_sets);
      emit_instr (fmt, (mh_instr_t) (long) R_REG);
      emit_string (fmt, node->v.str);
      break;

    case fmtnode_number:
      emit_opcode (fmt, mhop_setn);
      emit_instr (fmt, (mh_instr_t) (long) R_REG);
      emit_instr (fmt, (mh_instr_t) (long) node->v.num);
      break;

    case fmtnode_body:
      emit_opcode (fmt, mhop_ldbody);
      emit_instr (fmt, (mh_instr_t) (long) R_REG);
      break;

    case fmtnode_comp:
      emit_opcode (fmt, mhop_ldcomp);
      emit_instr (fmt, (mh_instr_t) (long) R_REG);
      emit_string (fmt, node->v.str);
      break;

    case fmtnode_funcall:
      emit_funcall (fmt, node->v.funcall.builtin, node->v.funcall.arg);
      break;

    case fmtnode_cntl:
      {
	long pc[2];
	
	/* Implementation of control escapes is a bit tricky. According to
	   the spec:
	   
	     "[f]unction escapes write their return value in 'num' for
	      functions returning integer or boolean values"

	   That means that after "%<(gt 1024)" the value of 'num' would be
	   1 or 0, depending on its value prior to entering the conditional.
	   However this would defeat the purpose of the conditional itself,
	   because then the following construct would be meaningless:

	       %<(gt 1024)...%?(gt 512)...%|...%>

	   Indeed, in MH implementation the value of 'num' propagates into
	   the conditional expression, because any function escape serving
	   as condition is evaluated in a separate context.

	   To ensure this behavior, the virtual machine of GNU MH holds the
	   value of the 'num' register on stack while evaluating the condition
	   and restores it afterward.

	   On the other hand, the spec says that:

 	     "[c]ontrol escapes return a boolean value, setting num to 1
	     if the last explicit condition evaluated by a `%<'  or `%?'
	     control succeeded, and 0 otherwise."

	   To ensure this, the value on top of stack is exchanged with the
	   value of the 'num' register upon entering the 'if' branch, and
	   the tos value is popped into the 'num' upon leaving it. Any
	   'else if' branches are handled the same way.

	   Before leaving the 'else' branch, the 'num' is set to 0 explicitly.
	*/
	emit_opcode (fmt, mhop_pushn);
	codegen_node (fmt, node->v.cntl.cond);
	emit_opcode_typed (fmt, node->v.cntl.cond->datatype,
			   mhop_brzn, mhop_brzs);
	pc[0] = fmt->progcnt;
	emit_instr (fmt, (mh_instr_t) NULL);
	if (node->v.cntl.iftrue)
	  {
	    emit_opcode (fmt, mhop_xchgn);
	    codegen_nodelist (fmt, node->v.cntl.iftrue);
	  }
	emit_opcode (fmt, mhop_popn);

	if (node->v.cntl.iffalse)
	  {
	    emit_opcode (fmt, mhop_branch);
	    pc[1] = fmt->progcnt;
	    emit_instr (fmt, (mh_instr_t) NULL);
	
	    fmt->prog[pc[0]].num = fmt->progcnt - pc[0];
	    emit_opcode (fmt, mhop_popn);
	    codegen_nodelist (fmt, node->v.cntl.iffalse);
	    if (node->v.cntl.iffalse->nodetype != fmtnode_cntl)
	      {
		emit_opcode (fmt, mhop_setn);
		emit_instr (fmt, (mh_instr_t) (long) R_REG);
		emit_instr (fmt, (mh_instr_t) (long) 0);
	      }
	    fmt->prog[pc[1]].num = fmt->progcnt - pc[1];
	  }
	else
	  fmt->prog[pc[0]].num = fmt->progcnt - pc[0];
      }
      break;

    case fmtnode_typecast:
      codegen_node (fmt, node->v.arg);
      switch (node->datatype)
	{
	case mhtype_num:
	  emit_opcode (fmt, mhop_atoi);
	  break;

	case mhtype_str:
	  emit_opcode (fmt, mhop_itoa);
	  break;

	default:
	  abort ();
	}
      break;

    default:
      abort ();
    }
}

static void
codegen_nodelist (struct mh_format *fmt, struct node *node)
{
  while (node)
    {
      codegen_node (fmt, node);
      node = node->next;
    }
}
	
static void
codegen (mh_format_t *fmtptr, int tree)
{
  struct mh_format *fmt;

  fmt = mu_zalloc (sizeof *fmt);
  
  *fmtptr = fmt;
  emit_opcode (fmt, mhop_stop);
  codegen_nodelist (fmt, parse_tree);
  emit_opcode (fmt, mhop_stop);
  
  if (tree)
    {
      fmt->tree = parse_tree;
      fmt->pool = tokpool;
    }
  else
    {
      node_list_free (parse_tree);
      mu_opool_destroy (&tokpool);
    }
}


