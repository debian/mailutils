/* GNU Mailutils -- a suite of utilities for electronic mail
   Copyright (C) 2024-2025 Free Software Foundation, Inc.

   GNU Mailutils is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Mailutils is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>. */

#include <mailutils/mailutils.h>

int owner_option;
int mode_option;
int force_option;
int deref_option;

static struct mu_option copy_options[] = {
  { "owner", 'u', NULL, MU_OPTION_DEFAULT,
    "copy ownership",
    mu_c_bool, &owner_option },
  { "mode", 'm', NULL, MU_OPTION_DEFAULT,
    "copy mode",
    mu_c_bool, &mode_option },
  { "force", 'f', NULL, MU_OPTION_DEFAULT,
    "force overwriting the destination file if it exists",
    mu_c_bool, &force_option },
  { "overwrite", 0, NULL, MU_OPTION_ALIAS },
  { "dereference", 'h', NULL, MU_OPTION_DEFAULT,
    "dereference symbolic links",
    mu_c_bool, &deref_option },
  MU_OPTION_END
}, *options[] = { copy_options, NULL };
  
struct mu_cli_setup cli = {
  options,
  NULL,
  "copy file",
  "SRC DST"
};

static char *capa[] = {
  "debug",
  NULL
};

int
main (int argc, char **argv)
{
  int rc;
  int flags;
  
  mu_cli (argc, argv, &cli, capa, NULL, &argc, &argv);

  if (argc != 2)
    {
      mu_error ("wrong number of arguments");
      return 1;
    }

  flags = (owner_option ? MU_COPY_OWNER : 0)
        | (mode_option ? MU_COPY_MODE : 0)
        | (force_option ? MU_COPY_OVERWRITE : 0)
        | (deref_option ? MU_COPY_DEREF : 0);
  rc = mu_copy_file (argv[0], argv[1], flags);

  if (rc)
    mu_diag_funcall (MU_DIAG_ERROR, "mu_copy_file", NULL, rc);

  return !!rc;
}

      
