/* GNU Mailutils -- a suite of utilities for electronic mail
   Copyright (C) 2024-2025 Free Software Foundation, Inc.

   GNU Mailutils is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GNU Mailutils is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>. */

#include <mailutils/mailutils.h>

int force_option;

static struct mu_option rename_options[] = {
  { "force", 'f', NULL, MU_OPTION_DEFAULT,
    "force overwriting the destination file if it exists",
    mu_c_bool, &force_option },
  { "overwrite", 0, NULL, MU_OPTION_ALIAS },
  MU_OPTION_END
}, *options[] = { rename_options, NULL };
  
struct mu_cli_setup cli = {
  options,
  NULL,
  "rename file",
  "SRC DST"
};

static char *capa[] = {
  "debug",
  NULL
};

int
main (int argc, char **argv)
{
  int rc;
  
  mu_cli (argc, argv, &cli, capa, NULL, &argc, &argv);

  if (argc != 2)
    {
      mu_error ("wrong number of arguments");
      return 1;
    }

  if (!mu_file_name_is_safe (argv[0])
      || (argv[0][0] == '/' && mu_str_count (argv[0], "/", NULL) < 2))
    {
      mu_error ("%s: unsafe file name", argv[0]);
      return 1;
    }
  if (!mu_file_name_is_safe (argv[1])
      || (argv[1][0] == '/' && mu_str_count (argv[1], "/", NULL) < 2))
    {
      mu_error ("%sunsafe file name", argv[0]);
      return 1;
    }
  
  rc = mu_rename_file (argv[0], argv[1], force_option ? MU_COPY_OVERWRITE : 0);

  if (rc)
    mu_diag_funcall (MU_DIAG_ERROR, "mu_rename_file", NULL, rc);

  return !!rc;
}

      
