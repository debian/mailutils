/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 17 "msgset.y"

#include "mail.h"

#include <stdio.h>
#include <stdlib.h>

/* Defined in <limits.h> on some systems, but redefined in <regex.h>
   if we are using GNU's regex. So, undef it to avoid duplicate definition
   warnings. */

#ifdef RE_DUP_MAX
# undef RE_DUP_MAX
#endif
#include <regex.h>

struct header_data
{
  char *header;
  char *expr;
};

static msgset_t *msgset_select (int (*sel) (mu_message_t, void *),
				     void *closure, int rev,
				     unsigned int max_matches);
static int select_header (mu_message_t msg, void *closure);
static int select_body (mu_message_t msg, void *closure);
static int select_sender (mu_message_t msg, void *closure);
static int select_deleted (mu_message_t msg, void *closure);
static int check_set (msgset_t **pset, int flags);

struct msgset_parser
{
  int flags;
  size_t message_count;
  msgset_t *result;
};

struct msgset_lexer
{
  int xargc;
  char **xargv;
  int cur_ind;
  char *cur_p;
  mu_opool_t tokpool;
};

int yyerror (struct msgset_parser *, struct msgset_lexer *, const char *);
int yylex  (struct msgset_lexer *);

static void
msgset_parser_free (struct msgset_parser *parser)
{
  msgset_free (parser->result);
}

static void
msgset_lexer_free (struct msgset_lexer *lexer)
{
  mu_opool_destroy (&lexer->tokpool);
}

static msgset_t *
msgset_concat (msgset_t *mset, msgset_t *part)
{
  msgset_t *ret = msgset_expand (mset, part);
  msgset_free (mset);
  msgset_free (part);
  return ret;
}
 
typedef int (*message_selector_t) (mu_message_t, void *);
static message_selector_t find_type_selector (int type);

#line 145 "msgset.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    TYPE = 258,                    /* TYPE  */
    IDENT = 259,                   /* IDENT  */
    REGEXP = 260,                  /* REGEXP  */
    HEADER = 261,                  /* HEADER  */
    BODY = 262,                    /* BODY  */
    NUMBER = 263                   /* NUMBER  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 96 "msgset.y"

  char *string;
  int number;
  int type;
  msgset_t *mset;

#line 207 "msgset.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (struct msgset_parser *parser, struct msgset_lexer *lexer);



/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_TYPE = 3,                       /* TYPE  */
  YYSYMBOL_IDENT = 4,                      /* IDENT  */
  YYSYMBOL_REGEXP = 5,                     /* REGEXP  */
  YYSYMBOL_HEADER = 6,                     /* HEADER  */
  YYSYMBOL_BODY = 7,                       /* BODY  */
  YYSYMBOL_NUMBER = 8,                     /* NUMBER  */
  YYSYMBOL_9_ = 9,                         /* '.'  */
  YYSYMBOL_10_ = 10,                       /* '^'  */
  YYSYMBOL_11_ = 11,                       /* '$'  */
  YYSYMBOL_12_ = 12,                       /* '*'  */
  YYSYMBOL_13_ = 13,                       /* '-'  */
  YYSYMBOL_14_ = 14,                       /* '+'  */
  YYSYMBOL_15_ = 15,                       /* ','  */
  YYSYMBOL_16_ = 16,                       /* '{'  */
  YYSYMBOL_17_ = 17,                       /* '}'  */
  YYSYMBOL_18_ = 18,                       /* '!'  */
  YYSYMBOL_19_ = 19,                       /* ']'  */
  YYSYMBOL_20_ = 20,                       /* '('  */
  YYSYMBOL_21_ = 21,                       /* ')'  */
  YYSYMBOL_22_ = 22,                       /* '['  */
  YYSYMBOL_YYACCEPT = 23,                  /* $accept  */
  YYSYMBOL_input = 24,                     /* input  */
  YYSYMBOL_msgset = 25,                    /* msgset  */
  YYSYMBOL_msgexpr = 26,                   /* msgexpr  */
  YYSYMBOL_msgspec = 27,                   /* msgspec  */
  YYSYMBOL_msg = 28,                       /* msg  */
  YYSYMBOL_header = 29,                    /* header  */
  YYSYMBOL_rangeset = 30,                  /* rangeset  */
  YYSYMBOL_range = 31,                     /* range  */
  YYSYMBOL_number = 32,                    /* number  */
  YYSYMBOL_partno = 33,                    /* partno  */
  YYSYMBOL_dot = 34,                       /* dot  */
  YYSYMBOL_obracket = 35                   /* obracket  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  29
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   99

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  23
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  13
/* YYNRULES -- Number of rules.  */
#define YYNRULES  38
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  54

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   263


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    18,     2,     2,    11,     2,     2,     2,
      20,    21,    12,    14,    15,    13,     9,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    22,     2,    19,    10,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    16,     2,    17,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   112,   112,   115,   119,   123,   127,   131,   135,   139,
     145,   146,   150,   156,   162,   166,   172,   173,   177,   181,
     184,   199,   208,   223,   235,   238,   244,   245,   249,   255,
     256,   274,   281,   282,   286,   292,   296,   302,   311
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "TYPE", "IDENT",
  "REGEXP", "HEADER", "BODY", "NUMBER", "'.'", "'^'", "'$'", "'*'", "'-'",
  "'+'", "','", "'{'", "'}'", "'!'", "']'", "'('", "')'", "'['", "$accept",
  "input", "msgset", "msgexpr", "msgspec", "msg", "header", "rangeset",
  "range", "number", "partno", "dot", "obracket", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-15)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-25)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      -2,   -15,   -15,   -15,   -15,    12,   -15,   -15,   -15,   -15,
     -15,   -15,    58,    58,    60,    32,    34,   -15,   -15,     8,
      46,   -15,   -15,     8,    51,    40,   -15,     7,   -15,   -15,
      58,   -15,   -15,   -15,    60,    60,   -15,    60,    60,   -15,
     -15,   -15,   -15,    60,   -15,   -15,   -15,    11,    67,    11,
      73,   -15,   -15,   -15
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       2,    22,    23,    25,    21,    35,     3,     5,     6,     7,
       8,     9,    24,    24,     0,     0,     4,    10,    13,    16,
       0,    19,    29,    32,     0,    24,    15,     0,    26,     1,
      24,    12,    37,    38,     0,     0,    20,     0,     0,    35,
      31,    30,    14,     0,    36,    28,    11,    17,     0,    33,
       0,    27,    18,    34
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -15,   -15,    33,    54,   -15,   -15,   -15,    61,   -14,    29,
     -15,    49,    50
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,    15,    16,    17,    18,    19,    20,    27,    21,    22,
      23,    34,    35
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int8 yytable[] =
{
      28,     1,     2,   -24,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    45,    12,     5,    13,    32,    14,     5,
      28,    28,    43,    28,    28,    24,    43,    14,    44,    51,
      33,    14,    29,    45,    45,    45,    45,     1,     2,   -24,
       3,     4,     5,     1,     2,    25,     3,     4,     5,    30,
      12,    36,    13,    41,    14,    30,    12,    42,    13,    39,
      14,     1,     2,    40,     3,     4,     5,    26,     5,     0,
      31,    14,    37,    38,    12,     5,    13,     0,    14,    31,
      14,     5,    43,     0,    46,     0,    52,    14,    43,     0,
       0,     0,    53,    14,     0,    47,    48,     0,    49,    50
};

static const yytype_int8 yycheck[] =
{
      14,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    27,    16,     8,    18,     9,    20,     8,
      34,    35,    15,    37,    38,    13,    15,    20,    21,    43,
      22,    20,     0,    47,    48,    49,    50,     3,     4,     5,
       6,     7,     8,     3,     4,    12,     6,     7,     8,    15,
      16,     5,    18,    24,    20,    15,    16,    17,    18,     8,
      20,     3,     4,    12,     6,     7,     8,    13,     8,    -1,
      16,    20,    23,    23,    16,     8,    18,    -1,    20,    25,
      20,     8,    15,    -1,    30,    -1,    19,    20,    15,    -1,
      -1,    -1,    19,    20,    -1,    34,    35,    -1,    37,    38
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,     4,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    16,    18,    20,    24,    25,    26,    27,    28,
      29,    31,    32,    33,    13,    25,    26,    30,    31,     0,
      15,    26,     9,    22,    34,    35,     5,    34,    35,     8,
      12,    32,    17,    15,    21,    31,    26,    30,    30,    30,
      30,    31,    19,    19
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    23,    24,    24,    24,    24,    24,    24,    24,    24,
      25,    25,    25,    26,    26,    26,    27,    27,    27,    27,
      28,    28,    28,    28,    29,    29,    30,    30,    30,    31,
      31,    31,    32,    32,    32,    33,    33,    34,    35
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     1,     1,     1,     1,     1,     1,     1,
       1,     3,     2,     1,     3,     2,     1,     3,     4,     1,
       2,     1,     1,     1,     0,     1,     1,     3,     2,     1,
       3,     3,     1,     3,     4,     1,     3,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (parser, lexer, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, parser, lexer); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, struct msgset_parser *parser, struct msgset_lexer *lexer)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (parser);
  YY_USE (lexer);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, struct msgset_parser *parser, struct msgset_lexer *lexer)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep, parser, lexer);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule, struct msgset_parser *parser, struct msgset_lexer *lexer)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)], parser, lexer);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, parser, lexer); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, struct msgset_parser *parser, struct msgset_lexer *lexer)
{
  YY_USE (yyvaluep);
  YY_USE (parser);
  YY_USE (lexer);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct msgset_parser *parser, struct msgset_lexer *lexer)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (lexer);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* input: %empty  */
#line 112 "msgset.y"
           {
	     parser->result = msgset_make_1 (get_cursor ());
	   }
#line 1261 "msgset.c"
    break;

  case 3: /* input: '.'  */
#line 116 "msgset.y"
           {
	     parser->result = msgset_make_1 (get_cursor ());
	   }
#line 1269 "msgset.c"
    break;

  case 4: /* input: msgset  */
#line 120 "msgset.y"
           {
	     parser->result = (yyvsp[0].mset);
	   }
#line 1277 "msgset.c"
    break;

  case 5: /* input: '^'  */
#line 124 "msgset.y"
           {
	     parser->result = msgset_select (select_deleted, lexer, 0, 1);
	   }
#line 1285 "msgset.c"
    break;

  case 6: /* input: '$'  */
#line 128 "msgset.y"
           {
	     parser->result = msgset_select (select_deleted, lexer, 1, 1);
	   }
#line 1293 "msgset.c"
    break;

  case 7: /* input: '*'  */
#line 132 "msgset.y"
           {
	     parser->result = msgset_select (select_deleted, lexer, 0, total);
	   }
#line 1301 "msgset.c"
    break;

  case 8: /* input: '-'  */
#line 136 "msgset.y"
           {
	     parser->result = msgset_select (select_deleted, lexer, 1, 1);
	   }
#line 1309 "msgset.c"
    break;

  case 9: /* input: '+'  */
#line 140 "msgset.y"
           {
	     parser->result = msgset_select (select_deleted, lexer, 0, 1);
	   }
#line 1317 "msgset.c"
    break;

  case 11: /* msgset: msgset ',' msgexpr  */
#line 147 "msgset.y"
           {
	     (yyval.mset) = msgset_append ((yyvsp[-2].mset), (yyvsp[0].mset));
	   }
#line 1325 "msgset.c"
    break;

  case 12: /* msgset: msgset msgexpr  */
#line 151 "msgset.y"
           {
	     (yyval.mset) = msgset_append ((yyvsp[-1].mset), (yyvsp[0].mset));
	   }
#line 1333 "msgset.c"
    break;

  case 13: /* msgexpr: msgspec  */
#line 157 "msgset.y"
           {
	     (yyval.mset) = (yyvsp[0].mset);
	     if (check_set (&(yyval.mset), parser->flags))
	       YYABORT;
	   }
#line 1343 "msgset.c"
    break;

  case 14: /* msgexpr: '{' msgset '}'  */
#line 163 "msgset.y"
           {
	     (yyval.mset) = (yyvsp[-1].mset);
	   }
#line 1351 "msgset.c"
    break;

  case 15: /* msgexpr: '!' msgexpr  */
#line 167 "msgset.y"
           {
	     (yyval.mset) = msgset_negate ((yyvsp[0].mset));
	   }
#line 1359 "msgset.c"
    break;

  case 17: /* msgspec: msg dot rangeset  */
#line 174 "msgset.y"
           {
	     (yyval.mset) = msgset_concat ((yyvsp[-2].mset), (yyvsp[0].mset));
	   }
#line 1367 "msgset.c"
    break;

  case 18: /* msgspec: msg obracket rangeset ']'  */
#line 178 "msgset.y"
           {
	     (yyval.mset) = msgset_concat ((yyvsp[-3].mset), (yyvsp[-1].mset));
	   }
#line 1375 "msgset.c"
    break;

  case 20: /* msg: header REGEXP  */
#line 185 "msgset.y"
           {
	     struct header_data hd;
	     hd.header = (yyvsp[-1].string);
	     hd.expr   = (yyvsp[0].string);
	     (yyval.mset) = msgset_select (select_header, &hd, 0, 0);
	     if (!(yyval.mset))
	       {
		 if ((yyvsp[-1].string))
		   mu_error (_("No applicable messages from {%s:/%s}"), (yyvsp[-1].string), (yyvsp[0].string));
		 else
		   mu_error (_("No applicable messages from {/%s}"), (yyvsp[0].string));
		 YYERROR;
	       }
	   }
#line 1394 "msgset.c"
    break;

  case 21: /* msg: BODY  */
#line 200 "msgset.y"
           {
	     (yyval.mset) = msgset_select (select_body, (yyvsp[0].string), 0, 0);
	     if (!(yyval.mset))
	       {
		 mu_error (_("No applicable messages from {:/%s}"), (yyvsp[0].string));
		 YYERROR;
	       }
	   }
#line 1407 "msgset.c"
    break;

  case 22: /* msg: TYPE  */
#line 209 "msgset.y"
           {
	     message_selector_t sel = find_type_selector ((yyvsp[0].type));
	     if (!sel)
	       {
		 yyerror (parser, lexer, _("unknown message type"));
		 YYERROR;
	       }
	     (yyval.mset) = msgset_select (sel, NULL, 0, 0);
	     if (!(yyval.mset))
	       {
		 mu_error (_("No messages satisfy :%c"), (yyvsp[0].type));
		 YYERROR;
	       }
	   }
#line 1426 "msgset.c"
    break;

  case 23: /* msg: IDENT  */
#line 224 "msgset.y"
           {
	     (yyval.mset) = msgset_select (select_sender, (void *)(yyvsp[0].string), 0, 0);
	     if (!(yyval.mset))
	       {
		 mu_error (_("No applicable messages from {%s}"), (yyvsp[0].string));
		 YYERROR;
	       }
	   }
#line 1439 "msgset.c"
    break;

  case 24: /* header: %empty  */
#line 235 "msgset.y"
           {
	     (yyval.string) = NULL;
	   }
#line 1447 "msgset.c"
    break;

  case 25: /* header: HEADER  */
#line 239 "msgset.y"
           {
	     (yyval.string) = (yyvsp[0].string);
	   }
#line 1455 "msgset.c"
    break;

  case 27: /* rangeset: rangeset ',' range  */
#line 246 "msgset.y"
           {
	     (yyval.mset) = msgset_append ((yyvsp[-2].mset), (yyvsp[0].mset));
	   }
#line 1463 "msgset.c"
    break;

  case 28: /* rangeset: rangeset range  */
#line 250 "msgset.y"
           {
	     (yyval.mset) = msgset_append ((yyvsp[-1].mset), (yyvsp[0].mset));
	   }
#line 1471 "msgset.c"
    break;

  case 30: /* range: NUMBER '-' number  */
#line 257 "msgset.y"
           {
	     if (msgset_length ((yyvsp[0].mset)) == 1)
	       {
		 if (((yyval.mset) = msgset_range ((yyvsp[-2].number), msgset_msgno ((yyvsp[0].mset)))) == NULL)
		   yyerror (parser, lexer, _("range error"));
	       }
	     else
	       {
		 (yyval.mset) = msgset_range ((yyvsp[-2].number), msgset_msgno ((yyvsp[0].mset)) - 1);
		 if (!(yyval.mset))
		   {
		     yyerror (parser, lexer, _("range error"));
		     YYERROR;
		   }
		 msgset_append ((yyval.mset), (yyvsp[0].mset));
	       }
	   }
#line 1493 "msgset.c"
    break;

  case 31: /* range: NUMBER '-' '*'  */
#line 275 "msgset.y"
           {
	     if (((yyval.mset) = msgset_range ((yyvsp[-2].number), total)) == NULL)
	       yyerror (parser, lexer, _("range error"));
	   }
#line 1502 "msgset.c"
    break;

  case 33: /* number: partno dot rangeset  */
#line 283 "msgset.y"
           {
	     (yyval.mset) = msgset_concat ((yyvsp[-2].mset), (yyvsp[0].mset));
	   }
#line 1510 "msgset.c"
    break;

  case 34: /* number: partno obracket rangeset ']'  */
#line 287 "msgset.y"
           {
	     (yyval.mset) = msgset_concat ((yyvsp[-3].mset), (yyvsp[-1].mset));
	   }
#line 1518 "msgset.c"
    break;

  case 35: /* partno: NUMBER  */
#line 293 "msgset.y"
           {
	     (yyval.mset) = msgset_make_1 ((yyvsp[0].number));
	   }
#line 1526 "msgset.c"
    break;

  case 36: /* partno: '(' rangeset ')'  */
#line 297 "msgset.y"
           {
	     (yyval.mset) = (yyvsp[-1].mset);
	   }
#line 1534 "msgset.c"
    break;

  case 37: /* dot: '.'  */
#line 303 "msgset.y"
           {
	     if (!(parser->flags & MSG_ALLOWPART))
	       {
		 yyerror (parser, lexer, _("message parts not allowed"));
		 YYERROR;
	       }
	   }
#line 1546 "msgset.c"
    break;

  case 38: /* obracket: '['  */
#line 312 "msgset.y"
           {
	     if (!(parser->flags & MSG_ALLOWPART))
	       {
		 yyerror (parser, lexer, _("message parts not allowed"));
		 YYERROR;
	       }
	   }
#line 1558 "msgset.c"
    break;


#line 1562 "msgset.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (parser, lexer, YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, parser, lexer);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, parser, lexer);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (parser, lexer, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, parser, lexer);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, parser, lexer);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 319 "msgset.y"


int
yyerror (struct msgset_parser *parser, struct msgset_lexer *lexer,
	 const char *s)
{
  mu_stream_printf (mu_strerr, "%s: ", lexer->xargv[0]);
  mu_stream_printf (mu_strerr, "%s", s);
  if (!lexer->cur_p)
    mu_stream_printf (mu_strerr, _(" near end"));
  else if (*lexer->cur_p == 0)
    {
      int i =  (*lexer->cur_p == 0) ? lexer->cur_ind + 1 : lexer->cur_ind;
      if (i == lexer->xargc)
	mu_stream_printf (mu_strerr, _(" near end"));
      else
	mu_stream_printf (mu_strerr, _(" near %s"), lexer->xargv[i]);
    }
  else
    mu_stream_printf (mu_strerr, _(" near %s"), lexer->cur_p);
  mu_stream_printf (mu_strerr, "\n");
  return 0;
}

int
yylex (struct msgset_lexer *lexer)
{
  if (lexer->cur_ind == lexer->xargc)
    return 0;
  if (!lexer->cur_p)
    lexer->cur_p = lexer->xargv[lexer->cur_ind];

  if (*lexer->cur_p == 0)
    {
      lexer->cur_ind++;
      lexer->cur_p = NULL;
      return yylex (lexer);
    }

  if (mu_isdigit (*lexer->cur_p))
    {
      yylval.number = strtoul (lexer->cur_p, &lexer->cur_p, 10);
      return NUMBER;
    }

  if (mu_isalpha (*lexer->cur_p))
    {
      char *p = lexer->cur_p;

      while (*lexer->cur_p && *lexer->cur_p != ',' && *lexer->cur_p != ':') 
	lexer->cur_p++;
      mu_opool_append (lexer->tokpool, p, lexer->cur_p - p);
      mu_opool_append_char (lexer->tokpool, 0);
      yylval.string = mu_opool_finish (lexer->tokpool, NULL);
      if (*lexer->cur_p == ':')
	{
	  ++lexer->cur_p;
	  return HEADER;
	}
      return IDENT;
    }

  if (*lexer->cur_p == '/')
    {
      char *p = ++lexer->cur_p;

      while (*lexer->cur_p && *lexer->cur_p != '/')
	lexer->cur_p++;

      mu_opool_append (lexer->tokpool, p, lexer->cur_p - p);
      mu_opool_append_char (lexer->tokpool, 0);
      yylval.string = mu_opool_finish (lexer->tokpool, NULL);

      if (*lexer->cur_p)
	lexer->cur_p++;

      return REGEXP;
    }

  if (*lexer->cur_p == ':')
    {
      lexer->cur_p++;
      if (*lexer->cur_p == '/')
	{
	  char *p = ++lexer->cur_p;
	  
	  while (*lexer->cur_p && *lexer->cur_p != '/')
	    lexer->cur_p++;

	  mu_opool_append (lexer->tokpool, p, lexer->cur_p - p);
	  mu_opool_append_char (lexer->tokpool, 0);
	  yylval.string = mu_opool_finish (lexer->tokpool, NULL);

	  if (*lexer->cur_p)
	    lexer->cur_p++;

	  return BODY;
	}
      if (*lexer->cur_p == 0)
	return 0;
      yylval.type = *lexer->cur_p++;
      return TYPE;
    }

  return *lexer->cur_p++;
}

int
msgset_parse (const int argc, char **argv, int flags, msgset_t **mset)
{
  int rc;
  struct msgset_parser parser;
  struct msgset_lexer lexer;

  lexer.xargc = argc;
  lexer.xargv = argv;
  lexer.cur_ind = 1;
  lexer.cur_p = NULL;
  mu_opool_create (&lexer.tokpool, MU_OPOOL_ENOMEMABRT);

  parser.flags = flags;
  mu_mailbox_messages_count (mbox, &parser.message_count);
  parser.result = NULL;

  rc = yyparse (&parser, &lexer);
  if (rc == 0)
    {
      if (!parser.result)
	{
	  util_noapp ();
	  rc = 1;
	}
      else
	{
	  if (parser.result->crd[1] > parser.message_count)
	    {
	      util_error_range (parser.result->crd[1]);
	      rc = 1;
	    }
	  else
	    {
	      *mset = parser.result;
	      parser.result = NULL;
	    }
	}
    }
  msgset_parser_free (&parser);
  msgset_lexer_free (&lexer);
  return rc;
}

void
msgset_free (msgset_t *msg_set)
{
  msgset_t *next;

  while (msg_set)
    {
      next = msg_set->next;
      free (msg_set->crd);
      free (msg_set);
      msg_set = next;
    }
}

size_t
msgset_count (msgset_t *set)
{
  size_t count = 0;
  for (; set; set = set->next)
    count++;
  return count;
}

/* Create a message set consisting of a single msg_num and no subparts */
msgset_t *
msgset_make_1 (size_t number)
{
  msgset_t *mp;

  if (number == 0)
    return NULL;
  mp = mu_alloc (sizeof (*mp));
  mp->next = NULL;
  if (mu_coord_alloc (&mp->crd, 1))
    mu_alloc_die ();
  mp->crd[1] = number;
  return mp;
}

msgset_t *
msgset_dup (const msgset_t *set)
{
  msgset_t *mp;
  mp = mu_alloc (sizeof (*mp));
  mp->next = NULL;
  if (mu_coord_dup (set->crd, &mp->crd))
    mu_alloc_die ();
  return mp;
}

/* Append message set TWO to the end of message set ONE. Take care to
   eliminate duplicates. Preserve the ordering of both lists. Return
   the resulting set.

   The function is destructive: the set TWO is attached to ONE and
   eventually modified to avoid duplicates.
*/
msgset_t *
msgset_append (msgset_t *one, msgset_t *two)
{
  msgset_t *last;

  if (!one)
    return two;
  for (last = one; last->next; last = last->next)
    {
      msgset_remove (&two, msgset_msgno (last));
    }
  last->next = two;
  return one;
}

int
msgset_member (msgset_t *set, size_t n)
{
  for (; set; set = set->next)
    if (msgset_msgno (set) == n)
      return 1;
  return 0;
}

void
msgset_remove (msgset_t **pset, size_t n)
{
  msgset_t *cur = *pset, **pnext = pset;

  while (1)
    {
      if (cur == NULL)
	return;
      if (msgset_msgno (cur) == n)
	break;
      pnext = &cur->next;
      cur = cur->next;
    }
  *pnext = cur->next;
  cur->next = NULL;
  msgset_free (cur);
}

msgset_t *
msgset_negate (msgset_t *set)
{
  size_t i;
  msgset_t *first = NULL, *last = NULL;

  for (i = 1; i <= total; i++)
    {
      if (!msgset_member (set, i))
	{
	  msgset_t *mp = msgset_make_1 (i);
	  if (!first)
	    first = mp;
	  else
	    last->next = mp;
	  last = mp;
	}
    }
  return first;
}

msgset_t *
msgset_range (int low, int high)
{
  int i;
  msgset_t *mp, *first = NULL, *last = NULL;

  if (low == high)
    return msgset_make_1 (low);

  if (low >= high)
    return NULL;

  for (i = 0; low <= high; i++, low++)
    {
      mp = msgset_make_1 (low);
      if (!first)
	first = mp;
      else
	last->next = mp;
      last = mp;
    }
  return first;
}

msgset_t *
msgset_expand (msgset_t *set, msgset_t *expand_by)
{
  msgset_t *i, *j;
  msgset_t *first = NULL, *last = NULL, *mp;

  for (i = set; i; i = i->next)
    for (j = expand_by; j; j = j->next)
      {
	mp = mu_alloc (sizeof *mp);
	mp->next = NULL;
	if (mu_coord_alloc (&mp->crd,
			    mu_coord_length (i->crd) + mu_coord_length (j->crd)))
	  mu_alloc_die ();
	memcpy (&mp->crd[1], &i->crd[1],
		mu_coord_length (i->crd) * sizeof i->crd[0]);
	memcpy (&mp->crd[1] + mu_coord_length (i->crd), &j->crd[1],
		mu_coord_length (j->crd) * sizeof j->crd[0]);

	if (!first)
	  first = mp;
	else
	  last->next = mp;
	last = mp;
      }
  return first;
}

msgset_t *
msgset_select (message_selector_t sel, void *closure, int rev,
	       unsigned int max_matches)
{
  size_t i, match_count = 0;
  msgset_t *first = NULL, *last = NULL, *mp;
  mu_message_t msg = NULL;

  if (max_matches == 0)
    max_matches = total;

  if (rev)
    {
      for (i = total; i > 0; i--)
	{
	  mu_mailbox_get_message (mbox, i, &msg);
	  if ((*sel) (msg, closure))
	    {
	      mp = msgset_make_1 (i);
	      if (!first)
		first = mp;
	      else
		last->next = mp;
	      last = mp;
	      if (++match_count == max_matches)
		break;
	    }
	}
    }
  else
    {
      for (i = 1; i <= total; i++)
	{
	  mu_mailbox_get_message (mbox, i, &msg);
	  if ((*sel) (msg, closure))
	    {
	      mp = msgset_make_1 (i);
	      if (!first)
		first = mp;
	      else
		last->next = mp;
	      last = mp;
	      if (++match_count == max_matches)
		break;
	    }
	}
    }
  return first;
}

int
select_header (mu_message_t msg, void *closure)
{
  struct header_data *hd = (struct header_data *)closure;
  mu_header_t hdr;
  char *contents;
  const char *header = hd->header ? hd->header : MU_HEADER_SUBJECT;

  mu_message_get_header (msg, &hdr);
  if (mu_header_aget_value (hdr, header, &contents) == 0)
    {
      if (mailvar_get (NULL, "regex", mailvar_type_boolean, 0) == 0)
	{
	  /* Match string against the extended regular expression(ignoring
	     case) in pattern, treating errors as no match.
	     Return 1 for match, 0 for no match.
	  */
          regex_t re;
          int status;
	  int flags = REG_EXTENDED;

	  if (mu_islower (header[0]))
	    flags |= REG_ICASE;
          if (regcomp (&re, hd->expr, flags) != 0)
	    {
	      free (contents);
	      return 0;
	    }
          status = regexec (&re, contents, 0, NULL, 0);
          free (contents);
	  regfree (&re);
          return status == 0;
	}
      else
	{
	  int rc;
	  mu_strupper (contents);
	  rc = strstr (contents, hd->expr) != NULL;
	  free (contents);
	  return rc;
	}
    }
  return 0;
}

int
select_body (mu_message_t msg, void *closure)
{
  char *expr = closure;
  int noregex = mailvar_get (NULL, "regex", mailvar_type_boolean, 0);
  regex_t re;
  int status = 0;
  mu_body_t body = NULL;
  mu_stream_t stream = NULL;
  int rc;
  
  if (noregex)
    mu_strupper (expr);
  else if (regcomp (&re, expr, REG_EXTENDED | REG_ICASE) != 0)
    return 0;

  mu_message_get_body (msg, &body);
  rc = mu_body_get_streamref (body, &stream);
  if (rc == 0)
    {
      char *buffer = NULL;
      size_t size = 0;
      size_t n = 0;
      
      while (status == 0
	     && (rc = mu_stream_getline (stream, &buffer, &size, &n)) == 0
	     && n > 0)
	{
	  if (noregex)
	    {
	      /* FIXME: charset */
	      mu_strupper (buffer);
	      status = strstr (buffer, expr) != NULL;
	    }
	  else
	    status = regexec (&re, buffer, 0, NULL, 0) == 0;
	}
      mu_stream_destroy (&stream);
      free (buffer);
      if (rc)
	mu_diag_funcall (MU_DIAG_ERROR, "mu_stream_getline", NULL, rc);
    }
  else
    mu_diag_funcall (MU_DIAG_ERROR, "mu_body_get_streamref", NULL, rc);
  
  if (!noregex)
    regfree (&re);

  return status;
}

int
select_sender (mu_message_t msg, void *closure)
{
  char *needle = (char*) closure;
  char *sender = sender_string (msg);
  int status = strcmp (sender, needle) == 0;
  free (sender);
  return status;
}

static int
select_type_d (mu_message_t msg, void *unused MU_ARG_UNUSED)
{
  mu_attribute_t attr;

  if (mu_message_get_attribute (msg, &attr) == 0)
    return mu_attribute_is_deleted (attr);
  return 0;
}

static int
select_type_n (mu_message_t msg, void *unused MU_ARG_UNUSED)
{
  mu_attribute_t attr;

  if (mu_message_get_attribute (msg, &attr) == 0)
    return mu_attribute_is_recent (attr);
  return 0;
}

static int
select_type_o (mu_message_t msg, void *unused MU_ARG_UNUSED)
{
  mu_attribute_t attr;

  if (mu_message_get_attribute (msg, &attr) == 0)
    return mu_attribute_is_seen (attr);
  return 0;
}

static int
select_type_r (mu_message_t msg, void *unused MU_ARG_UNUSED)
{
  mu_attribute_t attr;

  if (mu_message_get_attribute (msg, &attr) == 0)
    return mu_attribute_is_read (attr);
  return 0;
}

static int
select_type_s (mu_message_t msg, void *unused MU_ARG_UNUSED)
{
  mu_attribute_t attr;

  if (mu_message_get_attribute (msg, &attr) == 0)
    return mu_attribute_is_userflag (attr, MAIL_ATTRIBUTE_SAVED);
  return 0;
}

static int
select_type_t (mu_message_t msg, void *unused MU_ARG_UNUSED)
{
  mu_attribute_t attr;

  if (mu_message_get_attribute (msg, &attr) == 0)
    return mu_attribute_is_userflag (attr, MAIL_ATTRIBUTE_TAGGED);
  return 0;
}

static int
select_type_T (mu_message_t msg, void *unused MU_ARG_UNUSED)
{
  mu_attribute_t attr;

  if (mu_message_get_attribute (msg, &attr) == 0)
    return !mu_attribute_is_userflag (attr, MAIL_ATTRIBUTE_TAGGED);
  return 0;
}

static int
select_type_u (mu_message_t msg, void *unused MU_ARG_UNUSED)
{
  mu_attribute_t attr;

  if (mu_message_get_attribute (msg, &attr) == 0)
    return !mu_attribute_is_read (attr);
  return 0;
}

struct type_selector
{
  int letter;
  message_selector_t func;
};

static struct type_selector type_selector[] = {
  { 'd', select_type_d },
  { 'n', select_type_n },
  { 'o', select_type_o },
  { 'r', select_type_r },
  { 's', select_type_s },
  { 't', select_type_t },
  { 'T', select_type_T },
  { 'u', select_type_u },
  { '/', NULL }, /* A pseudo-entry needed for msgtype_generator only */
  { 0 }
};

static message_selector_t
find_type_selector (int type)
{
  struct type_selector *p;
  for (p = type_selector; p->func; p++)
    {
      if (p->letter == type)
	return p->func;
    }
  return NULL;
}

#ifdef WITH_READLINE
char *
msgtype_generator (const char *text, int state)
{
  /* Allowed message types, plus '/'. The latter can folow a colon,
     meaning body lookup */
  static int i;
  char c;

  if (!state)
    {
      i = 0;
    }
  while ((c = type_selector[i].letter))
    {
      i++;
      if (!text[1] || text[1] == c)
	{
	  char *s = mu_alloc (3);
	  s[0] = ':';
	  s[1] = c;
	  s[2] = 0;
	  return s;
	}
    }
  return NULL;
}
#endif

int
select_deleted (mu_message_t msg, void *closure)
{
  mu_attribute_t attr= NULL;
  int rc;
  struct msgset_lexer *lexer = closure;
  
  mu_message_get_attribute (msg, &attr);
  rc = mu_attribute_is_deleted (attr);
  return strcmp (lexer->xargv[0], "undelete") == 0 ? rc : !rc;
}

int
check_set (msgset_t **pset, int flags)
{
  int rc = 0;

  if (!*pset)
    {
      util_noapp ();
      return 1;
    }
  if (msgset_count (*pset) == 1)
    flags ^= MSG_SILENT;
  if (flags & MSG_NODELETED)
    {
      msgset_t *p = *pset, *prev = NULL;
      msgset_t *delset = NULL;

      while (p)
	{
	  msgset_t *next = p->next;
	  if (util_isdeleted (msgset_msgno (p)))
	    {
	      if ((flags & MSG_SILENT) && (prev || next))
		{
		  /* Mark subset as deleted */
		  p->next = delset;
		  delset = p;
		  /* Remove it from the set */
		  if (prev)
		    prev->next = next;
		  else
		    *pset = next;
		}
	      else
		{
		  mu_error (_("%lu: Inappropriate message (has been deleted)"),
			    (unsigned long) msgset_msgno (p));
		  /* Delete entire set */
		  delset = *pset;
		  *pset = NULL;
		  rc = 1;
		  break;
		}
	    }
	  else
	    prev = p;
	  p = next;
	}

      if (delset)
	msgset_free (delset);

      if (!*pset)
	rc = 1;
    }

  return rc;
}


#if 0
void
msgset_print (msgset_t *mset)
{
  int i;
  mu_printf ("(");
  mu_printf ("%d .", mset->msg_part[0]);
  for (i = 1; i < mset->npart; i++)
    {
      mu_printf (" %d", mset->msg_part[i]);
    }
  mu_printf (")\n");
}

int
main(int argc, char **argv)
{
  msgset_t *mset = NULL;
  int rc = msgset_parse (argc, argv, &mset);

  for (; mset; mset = mset->next)
    msgset_print (mset);
  return 0;
}
#endif
